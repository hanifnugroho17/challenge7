import {
  View,
  Text,
  Image,
  StyleSheet,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import React, {useState} from 'react';
import {COLORS} from '../../utils/COLORS';
import ProfileUserHeader from '../../components/Chat/ProfileUser/ProfileUserHeader';
import Input from '../../components/Input';
import Button from '../../components/Button';
import {firebase} from '@react-native-firebase/database';
import Toast from 'react-native-simple-toast';

export default function ProfileUser({route, navigation}) {
  const [name, setName] = useState('');
  const [bio, setBio] = useState('');

  const userData = route.params.userData;

  const myDB = firebase
    .app()
    .database(
      'https://challenge7-aa6b8-default-rtdb.asia-southeast1.firebasedatabase.app/',
    );

  const onUpdateRDB = () => {
    if (name == '' || bio == '') {
      Toast.show('Please Input All Field!');
      return false;
    }
    try {
      myDB
        .ref('/users/' + userData.id)
        .update({
          name: name,
          bio: bio,
        })
        .then(() => {
          myDB
            .ref('/users/' + userData.id)
            .once('value')
            .then(async snapshot => {
              let userData = snapshot.val();
              Toast.show('Updated Successfully!');
              setName('');
              setBio('');
              navigation.navigate('ChatList', {userData: userData});
            });
        });
    } catch (error) {
      Toast.show(error);
    }
  };

  return (
    <View style={styles.Container}>
      <ProfileUserHeader navigation={navigation} userData={userData} />
      <KeyboardAvoidingView
        style={styles.Main}
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
        <Image
          style={styles.Image}
          source={{
            uri: 'https://cdn-icons-png.flaticon.com/512/758/758771.png',
          }}
        />
        <Text style={styles.Name}>{userData.name}</Text>
        <Text style={styles.Bio}>{userData.bio}</Text>
        <Input
          iconName={'account-outline'}
          placeholder={'Name'}
          onChangeText={text => setName(text)}
        />
        <Input
          iconName={'card-account-details-outline'}
          placeholder={'Bio'}
          onChangeText={text => setBio(text)}
        />
        <Button caption={'UPDATED'} onPress={onUpdateRDB} />
      </KeyboardAvoidingView>
    </View>
  );
}

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: COLORS.backgroundColor,
  },
  Main: {
    flex: 1,
    alignItems: 'center',
  },
  Image: {
    marginTop: 10,
    width: 120,
    height: 120,
    borderRadius: 50,
  },
  Name: {
    fontFamily: 'Poppins-Bold',
    fontSize: 40,
    color: COLORS.text,
    marginTop: 10,
  },
  Bio: {
    fontFamily: 'Poppins-Medium',
    fontSize: 20,
    color: COLORS.text,
    marginBottom: 20,
  },
  Icon: {
    backgroundColor: COLORS.button,
    padding: 10,
    borderRadius: 15,
  },
});
