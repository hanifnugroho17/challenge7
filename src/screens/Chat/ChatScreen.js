import React, {useState, useEffect} from 'react';
import {StyleSheet, View, StatusBar, FlatList} from 'react-native';
import {COLORS} from '../../utils/COLORS';
import {firebase} from '@react-native-firebase/database';
import ChatScreenHeader from '../../components/Chat/ChatScreen/ChatScreenHeader';
import MsgComponent from '../../components/Chat/ChatScreen/MsgComponent';
import InputMessage from '../../components/Chat/ChatScreen/InputMessage';
import moment from 'moment';
import Toast from 'react-native-simple-toast';

export default function ChatScreen({route, navigation}) {
  const receiverData = route.params.receiverData;
  const userData = route.params.userData;

  const [msg, setMsg] = useState('');
  const [disabled, setDisabled] = useState(false);
  const [allChat, setAllChat] = useState([]);

  const myDB = firebase
    .app()
    .database(
      'https://challenge7-aa6b8-default-rtdb.asia-southeast1.firebasedatabase.app/',
    );

  useEffect(() => {
    const onChildAdd = myDB
      .ref('/messages/' + receiverData.roomId)
      .on('child_added', snapshot => {
        setAllChat(state => [snapshot.val(), ...state]);
      });
    // Stop listening for updates when no longer required
    return () =>
      myDB
        .ref('/messages' + receiverData.roomId)
        .off('child_added', onChildAdd);
  }, [receiverData.roomId]);

  const msgvalid = txt => txt && txt.replace(/\s/g, '').length;
  const sendMsg = () => {
    if (msg == '' || msgvalid(msg) == 0) {
      Toast.show('Message Empty!');
      return false;
    }
    setDisabled(true);
    let msgData = {
      roomId: receiverData.roomId,
      message: msg,
      from: userData?.id,
      to: receiverData.id,
      sendTime: moment().format(''),
      msgType: 'text',
    };

    const newReference = myDB.ref('/messages/' + receiverData.roomId).push();
    msgData.id = newReference.key;
    newReference.set(msgData).then(() => {
      let chatListupdate = {
        lastMsg: msg,
        sendTime: msgData.sendTime,
      };
      myDB
        .ref('/chatlist/' + receiverData?.id + '/' + userData?.id)
        .update(chatListupdate);
      myDB
        .ref('/chatlist/' + userData?.id + '/' + receiverData?.id)
        .update(chatListupdate);

      setMsg('');
      setDisabled(false);
    });
  };

  return (
    <View style={styles.Container}>
      <StatusBar
        barStyle="dark-content"
        backgroundColor={COLORS.backgroundColor}
      />
      <ChatScreenHeader
        data={receiverData}
        navigation={navigation}
        userData={userData}
      />
      <FlatList
        style={{flex: 1}}
        data={allChat}
        showsVerticalScrollIndicator={false}
        keyExtractor={(item, index) => index}
        inverted
        renderItem={({item}) => {
          return <MsgComponent sender={item.from == userData.id} item={item} />;
        }}
      />
      <InputMessage
        value={msg}
        onChangeText={val => setMsg(val)}
        disabled={disabled}
        onPress={() => sendMsg()}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: COLORS.backgroundColor,
  },
});
