import {
  View,
  Text,
  TouchableOpacity,
  Image,
  SafeAreaView,
  FlatList,
  StyleSheet,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {firebase} from '@react-native-firebase/database';
import {COLORS} from '../../utils/COLORS';
import Search from '../../components/Chat/Search';
import ChatListHeader from '../../components/Chat/ChatList/ChatListHeader';

export default function ChatList({route, navigation}) {
  const userData = route.params.userData;
  const [search, setSearch] = useState('');
  const [chatList, setChatList] = useState([]);
  const [chatListBackup, setChatListBackup] = useState([]);

  useEffect(() => {
    getChatlist();
  }, []);

  const searchUser = val => {
    setSearch(val);
    setChatList(chatListBackup.filter(it => it.name.match(val)));
  };

  const myDB = firebase
    .app()
    .database(
      'https://challenge7-aa6b8-default-rtdb.asia-southeast1.firebasedatabase.app/',
    );

  const getChatlist = async () => {
    myDB.ref('/chatlist/' + userData?.id).on('value', snapshot => {
      if (snapshot.val() != null) {
        setChatList(
          Object.values(snapshot.val()).filter(it => it.lastMsg != ''),
        );
        setChatListBackup(
          Object.values(snapshot.val()).filter(it => it.lastMsg != ''),
        );
      }
    });
  };

  const renderItem = ({item}) => (
    <TouchableOpacity
      style={styles.FlatListContainer}
      onPress={() =>
        navigation.navigate('ChatScreen', {
          receiverData: item,
          userData: userData,
        })
      }>
      <Image
        style={styles.Image}
        source={{
          uri: 'https://cdn-icons-png.flaticon.com/512/758/758771.png',
        }}
      />
      <View>
        <Text style={styles.Name}>{item.name}</Text>
        <View>
          <Text numberOfLines={1} style={styles.lastMsg}>
            {item.lastMsg}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
  return (
    <SafeAreaView style={styles.Container}>
      <ChatListHeader userData={userData} navigation={navigation} />
      <Text style={styles.Title}>Messages</Text>
      <Search onChangeText={val => searchUser(val)} value={search} />
      <FlatList
        showsVerticalScrollIndicator={false}
        keyExtractor={(item, index) => index.toString()}
        data={chatList.sort(
          (a, b) => new Date(b.sendTime) - new Date(a.sendTime),
        )}
        renderItem={renderItem}
      />
      <TouchableOpacity
        style={styles.Button}
        onPress={() =>
          navigation.navigate('DashboardUser', {userData: userData})
        }>
        <Icon name="account-multiple" size={25} color={COLORS.white} />
      </TouchableOpacity>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: COLORS.backgroundColor,
    paddingBottom: 40,
  },
  Title: {
    fontFamily: 'Poppins-Black',
    fontSize: 25,
    color: COLORS.text,
    textAlign: 'center',
    marginVertical: 20,
  },
  FlatListContainer: {
    backgroundColor: COLORS.card,
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 40,
    height: 75,
    marginBottom: 20,
    borderRadius: 18,
  },
  Image: {
    width: 50,
    height: 50,
    borderRadius: 50,
    marginHorizontal: 20,
  },
  Name: {
    fontFamily: 'Poppins-Bold',
    fontSize: 16,
    color: COLORS.text,
  },
  lastMsg: {
    fontFamily: 'Poppins-Medium',
    fontSize: 14,
    color: COLORS.text,
  },
  Button: {
    position: 'absolute',
    bottom: 40,
    right: 40,
    width: 50,
    height: 50,
    borderRadius: 25,
    backgroundColor: COLORS.button,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
