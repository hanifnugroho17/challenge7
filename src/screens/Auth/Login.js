import {
  View,
  Text,
  SafeAreaView,
  KeyboardAvoidingView,
  StatusBar,
  Alert,
  StyleSheet,
  Platform,
} from 'react-native';
import React, {useState} from 'react';
import Input from '../../components/Input';
import Button from '../../components/Button';
import Direction from '../../components/Auth/Direction';
import {COLORS} from '../../utils/COLORS';
import {firebase} from '@react-native-firebase/database';
import Toast from 'react-native-simple-toast';

export default function Login({navigation}) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const myDB = firebase
    .app()
    .database(
      'https://challenge7-aa6b8-default-rtdb.asia-southeast1.firebasedatabase.app/',
    );

  const onLogin = () => {
    auth()
      .signInWithEmailAndPassword(email, password)
      .then(() => {
        navigation.navigate('ChatList', {email: email});
      })
      .catch(error => {
        Alert.alert('Error', error.code);
      });
  };

  const onLoginRDB = () => {
    if (email == '' || password == '') {
      Toast.show('Please Input All Field!');
      return false;
    }
    try {
      myDB
        .ref('users/')
        .orderByChild('emailId')
        .equalTo(email)
        .once('value')
        .then(async snapshot => {
          if (snapshot.val() == null) {
            Toast.show('Invalid Email!');
            return false;
          }
          let userData = Object.values(snapshot.val())[0];
          if (userData?.password != password) {
            Toast.show('Invalid Password!');
            return false;
          }
          navigation.navigate('ChatList', {userData: userData});
        });
    } catch (error) {
      Toast.show(error);
    }
  };

  return (
    <SafeAreaView style={styles.Main}>
      <StatusBar backgroundColor={'#fff'} barStyle={'dark-content'} />
      <KeyboardAvoidingView
        style={styles.Container}
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
        <Text style={styles.Title}>My App</Text>
        <Input
          iconName={'email-outline'}
          placeholder={'Email'}
          onChangeText={text => setEmail(text)}
        />
        <Input
          iconName={'lock-outline'}
          placeholder={'Password'}
          onChangeText={text => setPassword(text)}
          secureTextEntry={true}
        />
        <View style={styles.Button}>
          <Button caption={'LOGIN'} onPress={onLoginRDB} />
        </View>
        <Direction
          navigation={navigation}
          caption={"Don't have any account? "}
          page={'Register'}
        />
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  Main: {
    flex: 1,
    backgroundColor: COLORS.backgroundColor,
    justifyContent: 'center',
  },
  Container: {
    alignItems: 'center',
  },
  Title: {
    fontFamily: 'Poppins-Bold',
    fontSize: 40,
    marginBottom: 40,
    color: COLORS.title,
  },
  Button: {
    flexDirection: 'row',
  },
});
