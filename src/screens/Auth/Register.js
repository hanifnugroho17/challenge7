import {
  Text,
  SafeAreaView,
  KeyboardAvoidingView,
  StatusBar,
  StyleSheet,
  Platform,
  Alert,
} from 'react-native';
import React, {useState} from 'react';
import Input from '../../components/Input';
import Button from '../../components/Button';
import Direction from '../../components/Auth/Direction';
import auth from '@react-native-firebase/auth';
import {firebase} from '@react-native-firebase/database';
import {COLORS} from '../../utils/COLORS';
import uuid from 'react-native-uuid';
import Toast from 'react-native-simple-toast';

export default function Register({navigation}) {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [bio, setBio] = useState('');

  const myDB = firebase
    .app()
    .database(
      'https://challenge7-aa6b8-default-rtdb.asia-southeast1.firebasedatabase.app/',
    );

  const onRegister = async () => {
    auth()
      .createUserWithEmailAndPassword(email, password)
      .then(() => {
        const userf = auth().currentUser;
        userf.updateProfile({displayName: name});
        Alert.alert('Success', `${name} Was Successful Created`);
      })
      .catch(error => {
        setLoading(false);
        Alert.alert('Oops', error.code, 'red');
      });
  };

  const onRegisterWithRDB = async () => {
    if (name == '' || email == '' || password == '' || bio == '') {
      Toast.show('Please Input All Field!');
      return false;
    }
    let data = {
      id: uuid.v4(),
      name: name,
      emailId: email,
      password: password,
      bio: bio,
    };
    try {
      myDB
        .ref('/users/' + data.id)
        .set(data)
        .then(() => {
          Toast.show('Register Successfully!');
          setName('');
          setEmail('');
          setPassword('');
          setBio('');
          navigation.navigate('Login');
        });
    } catch (error) {
      Toast.show(error);
    }
  };
  return (
    <SafeAreaView style={styles.Main}>
      <StatusBar backgroundColor={'#fff'} barStyle={'dark-content'} />
      <KeyboardAvoidingView
        style={styles.Container}
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
        <Text style={styles.Title}>My App</Text>
        <Input
          iconName={'account-outline'}
          placeholder={'Name'}
          onChangeText={text => setName(text)}
        />
        <Input
          iconName={'email-outline'}
          placeholder={'Email'}
          onChangeText={text => setEmail(text)}
        />
        <Input
          iconName={'lock-outline'}
          placeholder={'Password'}
          onChangeText={text => setPassword(text)}
          secureTextEntry={true}
        />
        <Input
          iconName={'card-account-details-outline'}
          placeholder={'Bio'}
          onChangeText={text => setBio(text)}
        />
        <Button caption={'REGISTER'} onPress={onRegisterWithRDB} />
        <Direction
          navigation={navigation}
          caption={'Join us before? '}
          page={'Login'}
        />
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  Main: {
    flex: 1,
    backgroundColor: COLORS.backgroundColor,
    justifyContent: 'center',
  },
  Container: {
    alignItems: 'center',
  },
  Title: {
    fontFamily: 'Poppins-Bold',
    fontSize: 40,
    marginBottom: 40,
    color: '#000',
  },
  New: {
    marginTop: 50,
  },
});
