import React from 'react';
import {View, TouchableOpacity, Text, StyleSheet} from 'react-native';
import {COLORS} from '../../utils/COLORS';

const Direction = ({navigation, caption, page}) => {
  return (
    <View style={styles.Container}>
      <Text style={styles.Text}>{caption}</Text>
      <TouchableOpacity onPress={() => navigation.navigate(page)}>
        <Text style={{...styles.Text, color: COLORS.lightblue}}>{page}</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  Container: {
    flexDirection: 'row',
    marginTop: 50,
  },
  Text: {
    fontFamily: 'Poppins-Bold',
    color: COLORS.text,
    fontSize: 15,
  },
});

export default Direction;
