//import liraries
import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {COLORS} from '../../../utils/COLORS';
import moment from 'moment';

// create a component
const TimeDelivery = ({sender, item}) => {
  return (
    <View
      style={{
        ...styles.Container,
        justifyContent: sender ? 'flex-end' : 'flex-start',
      }}>
      <Text
        style={{...styles.Time, color: sender ? COLORS.time : COLORS.time2}}>
        {moment(item.sendTime).format('LLL')}
      </Text>
      <Icon
        name={item.sendTime ? 'check-all' : 'check'}
        size={20}
        color={COLORS.icon}
      />
    </View>
  );
};

// define your styles
const styles = StyleSheet.create({
  Container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  Time: {
    fontFamily: 'Poppins-Medium',
    fontSize: 10,
    marginRight: 10,
  },
});

//make this component available to the app
export default TimeDelivery;
