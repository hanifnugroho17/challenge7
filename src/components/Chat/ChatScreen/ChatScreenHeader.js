//import liraries
import React from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {COLORS} from '../../../utils/COLORS';

const ChatHeader = ({data, navigation, userData}) => {
  return (
    <View style={styles.Container}>
      <TouchableOpacity
        onPress={() => navigation.navigate('ChatList', {userData: userData})}>
        <Icon
          style={styles.Icon2}
          name={'keyboard-backspace'}
          size={25}
          color={COLORS.icon}
        />
      </TouchableOpacity>
      <Image
        style={styles.Image}
        source={{
          uri: 'https://cdn-icons-png.flaticon.com/512/758/758771.png',
        }}
      />
      <View>
        <Text style={styles.Name}>{data.name}</Text>
        <Text style={styles.Bio}>{data.bio}</Text>
      </View>
    </View>
  );
};

// define your styles
const styles = StyleSheet.create({
  Container: {
    backgroundColor: COLORS.backgroundColor,
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
  Image: {
    width: 40,
    height: 40,
    borderRadius: 50,
    marginHorizontal: 15,
  },
  Name: {
    fontFamily: 'Poppins-Bold',
    fontSize: 16,
    color: COLORS.text,
  },
  Bio: {
    fontFamily: 'Poppins-Medium',
    fontSize: 14,
    color: COLORS.text,
  },
});

//make this component available to the app
export default ChatHeader;
