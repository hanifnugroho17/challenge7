//import liraries
import React from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {COLORS} from '../../../utils/COLORS';

const DashboardUserHeader = ({navigation, userData}) => {
  return (
    <View style={styles.Container}>
      <Image
        style={styles.Image}
        source={{
          uri: 'https://cdn-icons-png.flaticon.com/512/758/758771.png',
        }}
      />
      <View>
        <Text style={styles.Name}>{userData.name}</Text>
        <Text style={styles.Bio}>{userData.bio}</Text>
      </View>
      <TouchableOpacity
        onPress={() =>
          navigation.navigate('ProfileUser', {userData: userData})
        }>
        <Icon
          style={styles.Icon}
          name={'account'}
          size={30}
          color={COLORS.white}
        />
      </TouchableOpacity>
    </View>
  );
};

// define your styles
const styles = StyleSheet.create({
  Container: {
    backgroundColor: COLORS.backgroundColor,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingHorizontal: 40,
    paddingTop: 20,
  },
  Image: {
    width: 50,
    height: 50,
    borderRadius: 50,
  },
  Name: {
    fontFamily: 'Poppins-Bold',
    fontSize: 20,
    color: COLORS.text,
  },
  Bio: {
    fontFamily: 'Poppins-Medium',
    fontSize: 16,
    color: COLORS.text,
  },
  Icon: {
    backgroundColor: COLORS.button,
    padding: 10,
    borderRadius: 15,
  },
});

//make this component available to the app
export default DashboardUserHeader;
